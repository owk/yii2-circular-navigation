#Yii2 Circular navigation
Created by Codrops

[http://www.codrops.com](http://www.codrops.com)

>Integrate or build upon it for free in your personal or commercial projects. Don't republish, redistribute or sell "as-is". 

Read more here: [licensing](http://tympanus.net/codrops/licensing/)

edited by owk
## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist owk/yii2-circular-navigation
```

or add

```
"owk/yii2-circular-navigation": "^1.0.0"
```

to the require section of your `composer.json` file.


## Usage

Once the extension is installed, simply use it in your view by  :
```php
<?php echo owk\circularnavigation\CircularNavigation::widget([
                'cnoptions'=>[
                    'menu' =>[
                        'background_color' => '#0088cd',
                        'background_color_hover' => 'rgb(12, 135, 212)',
                        // 'openLabel' => 'Menú',
                        // 'closeLabel' => 'Cerrar',
                        // 'color' => 'white'
                    ],
                    'item' => [
                        'background_color' => '-webkit-linear-gradient(90deg, #666666 0%,#319ed8 50%,#666666 100%)',
                        // 'background_color' => '#000',
                        // 'background_color_hover' => '#0088cd'
                    ]
                ],
                'items' => [
                    
                    [
                        'url' =>'#',
                        'icon' =>'glyphicon-list-alt'
                    ],
                    [
                        'url' =>'/',
                        'icon' =>'glyphicon-home'
                    ],
                    [
                        'url' =>'#editar',
                        'icon' =>'glyphicon-pencil'
                    ],
                    // [
                    //     'url' =>'#enviar',
                    //     'icon' =>'glyphicon-send'
                    // ],
                    [
                        'url' =>'#table',
                        'icon' =>'glyphicon-duplicate'
                    ]
                    ,
                    [
                        'url' =>'#aceptar',
                        'icon' =>'glyphicon-phone'
                    ]
                ]
                ]);?>
```