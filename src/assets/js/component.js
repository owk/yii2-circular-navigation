$(function(){
    $open = false;

    $wrapper = $('#cn-wrapper');
    $overlay = $('#cn-overlay');
    $menu = $('#cn-button');

    $menu.click(function(e){
        if ($open) {
            closeCN();
        }else{
            openCN();
        }
    });

    $overlay.click(function(e){
       closeCN();
    });
    init();
});
function init(){
    $('#cn-button').css("background",cnOptions.menu.background_color);
    $('#cn-button').hover(function(){ 
        $(this).css("background",cnOptions.menu.background_color_hover);
    },function(){
        $(this).css("background",cnOptions.menu.background_color);
    });

    $('#cn-button').css( "color",cnOptions.menu.color);
    $('#cn-button').text(cnOptions.menu.openLabel ? cnOptions.menu.openLabel : '+');
    //hover effect
    $('#cn-wrapper li a').css('background',cnOptions.item.background_color);
    $('#cn-wrapper li a').hover(function(){
        $(this).css('background-position','0px 140px');
        $(this).css('background',cnOptions.item.background_color_hover);
    },
        function(){
        $(this).css('background',cnOptions.item.background_color);}
    );
}
function openCN(){
    $open = true;
    $menu.text(cnOptions.menu.closeLabel ? cnOptions.menu.closeLabel : "-");
    $wrapper.addClass('opened-nav');
    $overlay.addClass('on-overlay');
    
}
function closeCN(){
    $open = false;
    $menu.text(cnOptions.menu.openLabel ? cnOptions.menu.openLabel : '+');
    $wrapper.removeClass('opened-nav');
    $overlay.removeClass('on-overlay');
}