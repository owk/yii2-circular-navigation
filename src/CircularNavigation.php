<?php 
namespace owk\circularnavigation;

use yii\base\Widget;

class CircularNavigation extends Widget{
    
    public $cnoptions = [];
    public $cnoptionsDefault = [
        'menu' => [
            'openLabel' => '+',
            'closeLabel' => '-',
            'color' => 'white',
            'background_color' => '#111',
        ],
        'item' => [
            'color' => 'white',
            'background_color' => '#111',
            'background_color_hover' => '#ccc',
        ]
    ];
    public $items = [];
    public $centerAngle;


    public function init()
    {
        parent::init();
        $this->cnoptions = array_merge($this->cnoptionsDefault,$this->cnoptions);
        
        //at least 2 items must be passed
        $this->centerAngle = 180 / count($this->items);
    }

    public function run(){
        return $this->render('nav',[
            'options' => $this->cnoptions,
            'items' => $this->items,
            'centerAngle' => $this->centerAngle,
        ]);
    }
}