<?php
use owk\circularnavigation\CircularNavigationAsset;
CircularNavigationAsset::register($this);
?>
<div class="csstransforms ">
    <script>
        var cnOptions = <?= json_encode($options)?>;
    </script>
    <button type="button"class="cn-button" id="cn-button"></button>
    <div class="cn-wrapper" id="cn-wrapper">
    <ul>
    <?php foreach ($items as $key => $value) :?>
        <li style="transform: rotate(<?= ($centerAngle*$key)?>deg) skew(<?= (90-$centerAngle)?>deg);">
            <a style="transform: skew(-<?= (90-$centerAngle)?>deg) rotate(-<?= (90-$centerAngle/2)?>deg);" href="<?= $value['url']?>"><span class="glyphicon <?= $value['icon']?>"></span></a>
        </li>
    <?php endforeach;?>
    </ul>
    </div>
    <div id="cn-overlay" class="cn-overlay"></div>
</div>