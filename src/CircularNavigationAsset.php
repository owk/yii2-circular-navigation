<?php

namespace owk\circularnavigation;

use yii\web\AssetBundle;

class CircularNavigationAsset extends AssetBundle
{
    public $sourcePath = '@owk/circularnavigation/assets';
    public $css = [
        'css/component.css',
    ];
    public $js = [
        'js/component.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap5\BootstrapAsset',
    ];
}
